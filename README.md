				    Projet final C/C++

Le but de ce projet est de constituer un système de génération de logs. Ces logs seront
destinés à être utilisés par des DataScientists.
Afin de mieux présenter les éléments, on commencera par définir les composantes de
l'architecture globale. Ensuite, on présentera les différents composants utilisés. Pour finir, on
présentera les livrables à fournir lors de la soutenance.

Le système de collecte des données est découpé en plusieurs parties :
Dans un premier temps, on distingue 2 Raspberry interconnectés en utilisant le protocole
TX/RX.
D'un côté, l'un des Raspberry qu'on appellera "Balise" doté de différents capteurs se charge
de collecter régulièrement les informations fournies par les capteurs.
De l'autre côté, le second Raspberry qu'on appellera "Maître" via le protocole TX/RX
récupère ces données.
Il va de soi qu'en cas de redémarrage de la "Balise", le programme de collecte des
informations devra se relancer automatiquement.
Concernant le "Maître", l'intervalle de collecte doit rester paramétrable. A ces données
issues des capteurs, on y ajoutera les caractéristiques liées à la balise elle-même.
Une fois ces données récupérées par le "Maître", celles-ci doivent être formatées au format
json en vue de les envoyer à un serveur TCP.
Ce serveur TCP centralisera les différentes données transmises par le "Maître" à intervalle
régulier afin de permettre d'établir des statistiques exploitables par les dataScientists.

Par ailleurs, le Maître sera doté de 2 servo-moteurs positionnés sur les connecteurs 1 et 2 de
la carte Contrôleur. Chaque servo moteur sera doté d'un drapeau. Sur l'un, on dispose d'un drapeau
de couleur Jaune, et sur l'autre de couleur Bleue.
Chaque servo-moteur aura 5 positions :
Servo Moteur
Drapeau Jaune

1
1 : 0°
2 : 45°
3 : 90 °
4 : 135°
5 : 180°

5
➔ Position Repos (Aucune présence détectée)
➔ Néant
➔ Détection Présence
➔ Néant
➔Position Repos (Après Présence détectée)

Servo Moteur
Drapeau Bleu

1
1 : 0°
2 : 45°
3 : 90 °
4 : 135°
5 : 180°

5
➔ Néant
➔ Détection de Nouvelles données (par le Maître) Position 2 vers Position 4
➔ Transfert vers Serveur TCP
➔ Détection de Nouvelles données (par le Maître) Position 4 vers Position 2
➔ Position Repos (Etat normal)

De plus, en vue de faciliter l'exploitation des données par les Datascientists, vous
constituerez une application console exposant les différentes informations issues du serveur TCP.
Au sein de cette application console, on pourrait par exemple :
- Obtenir les informations à un instant donné
o d'un capteur
o ou d'une information particulière du capteur
- Extraire des informations pour une période donnée
- …
Il s'agit ici de simples suggestions, à vous de fournir une application console regroupant des
fonctionnalités utiles aux datascientists. Cette application doit être simple et conviviale.

Voici les détails concernant les différents éléments :
- Raspberry Pi 3 Model B+
- Capteur d'humidité BME280
- Capteur Triple axis-boussole magnétomètre - HMC5883L ou GY-521
- Détecteur de mouvement infrarouge HC-SR501
- Carte PCA9685 Contrôleur de Servo Moteur
-

Raspberry Pi 3 Model B+ :
o Marque : U:Create
o Processeur : ARM
o Vitesse du processeur : 1.40 GHz
o Nombre de cœurs : 4
o Taille de la mémoire vive : 1GB
o Type de technologie sans fil : 802.11bgn, 802.11ac
o Nombre de ports USB 2.0 : 4

-

Capteurs d'humidité BME280 :
o capteur
environnemental
intégré
développé
spécifiquement
pour
les
applications mobiles où la
taille
et
la
faible
consommation d'énergie sont
des

o

-

contraintes de conception
essentielles
Tension d'alimentation : 1,8 5V DC

o
o
o
o
o
o
o
o
o
o
o
o
o
o

Interface : I2C (up to 3.4MHz),
SPI (up to 10 MHz)
Température : -40 à + 85 ° C
Humidité : 0-100%
Pression : 300-1100 hPa
Température : 0.01 ° C
Humidité : 0.008%
Type de Processeur : ARM710
Pression : 0.18Pa
Température : + -1 ° C
Humidité : + -3%
Pression : + -1Pa
Adresse I2C
SDO LOW : 0x76
SDO HIGH : 0x77

Capteur Triple axis-boussole magnétomètre - HMC5883L / GY-521
o Les données sont calculées simultanément en x, y et z grâce à
des convertisseurs 16-bits analogue à digital pour chaque
canal.
o Le capteur mesure également la température.
o Le capteur communique avec le protocole I2C.

-

Détecteur de mouvement infrarouge HC-SR501
o Tension de fonctionnement : 5 … 20 V c.c.
o Courant de repos : 65 µA
o Niveau de tension de sortie : 3,3 V haut/0 V bas
o Temps de retardement réglable (0,3 à 18 secondes)
o Temps de commutation (réglable) : 5 à 200 secondes
o Portée (réglable) : 3 à 7 mètres
o Temps de blocage : 2,5 secondes
o Plage de détection : 140°
o Température de fonctionnement : -15 à 70°
o Dimensions de la platine : 32 x 24 mm
o Écartement des trous de vis : 28 mm
o Diamètre de trou : 2 mm
o Diamètre de la lentille du capteur : 23 mm

-

Carte PCA9685 Contrôleur de Servo Moteur
o Alimentation 5V
o Communication par bus i2c
o 6 broches d’adresses (permet d’utiliser jusqu’à 62 PCA9685)
o Fréquence des signaux de sortie de 24Hz à 1526Hz
o Rapport cyclique codé sur 12bits (de 0% à 100%)
allant de 0 à 4095

Afin de disposer de ces données, vous aurez la possibilité de vous connecter via le protocole
SSH. Toutes les informations de connexion (adresse IP, login et mot de passe, clé) vous seront
communiquées ultérieurement.
L’ensemble des travaux réalisés devra être mis sur un dépôt afin de centraliser les
informations et de faciliter la communication entre les membres du projet. Vous ferez en sorte de
bien vous répartir les tâches.
Il va de soi que le code doit être respecté les règles d'écriture. Vous devrez générer une
documentation.
Vous constituerez une présentation (PowerPoint ou équivalent) qui vous servira de fil
conducteur pour le jour de la soutenance. Lors de la soutenance, il est évident que le temps de
parole entre chacun des membres du groupe devra être équilibré.

Bon courage.

